from __future__ import print_function
import logging
from pprint import pprint
from collections import namedtuple, defaultdict, MutableMapping, MutableSequence
import json
import sys
from itertools import islice
sys.path.append(".")
sys.path.append("./MCEdit/")
logging.basicConfig(level=logging.ERROR)
from MCEdit import pymclevel
from deepdiff import DeepDiff


def get_chests(filename):
    level = pymclevel.fromFile(filename)
    tileEntities = level.getTileEntitiesInBox(level.bounds)
    return [te for te in tileEntities if te['id'].value == u'minecraft:chest' and u'Items' in te]

def de_TAG(TAG):
    if isinstance(TAG, pymclevel.nbt.TAG_Compound):
        return {TAG[key].name: de_TAG(TAG[key].value) for key in TAG.keys()}
    elif isinstance(TAG, pymclevel.nbt.TAG_List):
        if TAG.name:
            return {TAG.name: [de_TAG(tag) for tag in TAG]}
        return [de_TAG(tag) for tag in TAG]
    elif not isinstance(TAG, pymclevel.nbt.TAG_Value):
        if isinstance(TAG, list):
            return [de_TAG(tag) for tag in TAG]
        return TAG
    else:
        if TAG.name:
            return {TAG.name: TAG.value}
        else:
            return TAG.value

def get_items(filename):
    chests = get_chests(filename)
    items = defaultdict(lambda: defaultdict(defaultdict))
    for chest in chests:
        items[chest['x'].value][chest['y'].value][chest['z'].value] = chest['Items']
    return {xk: {yk: {zk: de_TAG(zv)
                      for zk, zv in yv.items()}
                 for yk, yv in xv.items()}
            for xk, xv in items.items()}


new_items = get_items("./JustAWorld")
old_items = get_items("./JustAWorld.bak")

pprint(DeepDiff(old_items, new_items))


with open('new_items', 'w') as f:
    json.dump(new_items, f)

with open('old_items', 'w') as f:
    json.dump(old_items, f)

# print(tileEntities)

